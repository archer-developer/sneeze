const URL = 'success.mp3';

let yodelBuffer;
let context;

const AudioContext = window.AudioContext // Default
    || window.webkitAudioContext // Safari and old versions of Chrome
    || false;

if (AudioContext) {
    context = new AudioContext();

    window.fetch(URL)
        .then(response => response.arrayBuffer())
        .then(arrayBuffer => context.decodeAudioData(arrayBuffer))
        .then(audioBuffer => {
            yodelBuffer = audioBuffer;
        });
}

export default function play() {
    if (context) {
        const source = context.createBufferSource();
        source.buffer = yodelBuffer;
        source.connect(context.destination);
        source.start();
    }
}
