import React from 'react';
import play from './sound';
import './App.css';

const useStickyState = (key = "sticky", initialState = null) => {
  const [state, setState] = React.useState(() => {
    const storedState = localStorage.getItem(key);

    return storedState ? JSON.parse(storedState) : initialState;
  });

  React.useEffect(() => {
    localStorage.setItem(key, JSON.stringify(state));
  }, [key, state]);

  const clearState = () => localStorage.removeItem(key);

  return [state, setState, clearState];
};

export default function App() {

  const [state, setState] = useStickyState('state', {
    counters: {},
  });

  const getCurrentCounter = () => {
    const {counters} = state;
    const now = (new Date());
    const today = now.getFullYear()+'-'+now.getMonth()+'-'+now.getDate();

    return {
      counter: today in counters ? parseInt(counters[today]) : 0,
      key: today,
    };
  };

  const sneeze = () => {
    const {counters} = state;
    const {counter, key} = getCurrentCounter();

    counters[key] = counter + 1;
    setState({
      counters,
    });

    play();
  };

  const {counter} = getCurrentCounter();

  return (
      <div id="wrapper">
        <div className="counter">{counter}</div>
        <a href="#" onClick={sneeze} className="my-super-cool-btn">
          <div className="dots-container">
            <div className="dot"/>
            <div className="dot"/>
            <div className="dot"/>
            <div className="dot"/>
          </div>
          <span>Sneeze!</span>
        </a>
      </div>
  );
}
